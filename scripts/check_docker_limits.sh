#!/bin/bash

which jq >/dev/null 2>&1
if [ $? -eq 1 ]; then
  echo -e "Install jq!\n"
  exit 1
fi

echo Working ...
TOKEN=$(curl -s "https://auth.docker.io/token?service=registry.docker.io&scope=repository:ratelimitpreview/test:pull" | jq -r .token)
curl --head -H "Authorization: Bearer $TOKEN" https://registry-1.docker.io/v2/ratelimitpreview/test/manifests/latest

